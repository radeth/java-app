from django.db import models


# Create your models here.
class Project(models.Model):
    project_name = models.CharField(max_length=200)
    site_url = models.CharField(max_length=200)
    site_repo_url = models.CharField(max_length=200)
